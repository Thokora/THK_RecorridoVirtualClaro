﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    //[SerializedField] Depencencies;
    void Start()
    {
        
    }

    private void Initialize()
    {
        
    }
    void ChangeGameState(GameState gameState)
    {
        switch (gameState)
        {
            case GameState.Initialization:
                break;
            case GameState.Instructions:
                break;
            case GameState.StartGameplay:
                break;
            case GameState.EndGameplay:
                break;
            default: break;
        }
    }
}