﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class TransitionFader : MonoBehaviour
{
    public static Action faderFx;

    [SerializeField] private Image imageToFade;
    [SerializeField] private float FadeRate;

    bool starFading;

    Color imageColor;
    float targetAlpha = 1.0f;

    private void Awake()
    {
        if (faderFx == null)
        {
            faderFx += EnableFading;
        }
    }


    void Update()
    {
        if (starFading)
        {
            imageColor = this.imageToFade.color;
            float alphaDiff = Mathf.Abs(imageColor.a - this.targetAlpha);
            if (alphaDiff > 0.0001f)
            {
                imageColor.a = Mathf.Lerp(imageColor.a, targetAlpha, this.FadeRate * Time.deltaTime);
                imageToFade.color = imageColor;
            }
        }
    }
    private void EnableFading()
    {
        FadeIn();
        StartCoroutine(WaitFading());
    }

    IEnumerator WaitFading()
    {
        yield return new WaitUntil(() => imageColor.a >= 0.9);

        yield return new WaitForSeconds(1f);
        FadeOut();
    }

    public void FadeOut()
    {
        imageColor.a = 1f;
        this.targetAlpha = 0.0f;
    }

    public void FadeIn()
    {
        imageColor.a = 0f;
        this.targetAlpha = 1.0f;
        starFading = true;
    }
}
