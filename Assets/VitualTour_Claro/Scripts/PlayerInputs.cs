﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInputs : MonoBehaviour
{
    [SerializeField] private Camera playerCamera;
    [SerializeField] private bool useMouseToLook;

    private CharacterController characterController;
    [SerializeField] private float movementSpeed = 10;
    [SerializeField] private float rotationSpeed = 180;

    private Vector3 HorizontalRotation;
    private Vector3 ZMovement;


    public float gravity = 9.8f;
    private float gravityForce = 0;

    public float speedH = 2.0f;
    public float speedV = 2.0f;

    private float yaw = 0.0f;
    private float pitch = 0.0f;


    private void Awake()
    {
        if (useMouseToLook && playerCamera == null)
        {
            playerCamera = Camera.main;
        }

        characterController = GetComponent<CharacterController>();
    }
    public void Update()
    {
        float horizontalInput = Input.GetAxisRaw("Horizontal");
        HorizontalRotation = new Vector3(0, Input.GetAxisRaw("Horizontal") * rotationSpeed * Time.deltaTime, 0);
        ZMovement = new Vector3(0, 0, Input.GetAxisRaw("Vertical") * Time.deltaTime);
        
        ZMovement = gameObject.transform.TransformDirection(ZMovement);

        characterController.Move(ZMovement * movementSpeed);
        gameObject.transform.Rotate(HorizontalRotation);

        if (characterController.isGrounded)
        {
            gravityForce = 0;
        }
        else
        {
            gravityForce -= gravity * Time.deltaTime;
            characterController.Move(new Vector3(0, gravityForce, 0));
        }

        if (useMouseToLook)
        {
            if (Input.GetMouseButtonDown(1))
            {
                Cursor.lockState = CursorLockMode.Locked;


            }

            if (Input.GetMouseButton(1))
            {
                pitch -= speedV * Input.GetAxis("Mouse Y");
                playerCamera.transform.eulerAngles = new Vector3(pitch, gameObject.transform.eulerAngles.y, 0.0f);
            }

            if (Input.GetMouseButtonUp(1))
            {
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
            }

        }
    }

}
