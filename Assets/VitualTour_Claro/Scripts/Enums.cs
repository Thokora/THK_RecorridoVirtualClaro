﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState
{
    Initialization, 
    Instructions,
    StartGameplay, 
    EndGameplay
}

public enum Signs
{
    Ingresar = 0,
    Video1 = 1,
    Video2 = 2,
    Video3 = 3,
    Video4 = 4,
    Video5 = 5,
    Video6 = 6,
    Video7 = 7,
    Video8 = 8,
    Video9 = 9,
    Video10 = 10,
    Video11 = 11,
    Video12 = 12,
    Video13 = 13, 
    Salir = 14
}
