﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class Teleporter : MonoBehaviour
{
    [SerializeField] private bool keepCurrentObject = false;
    private GameObject playerObject;
    private GameObject currentObject;
    [SerializeField] private GameObject destinationObject;
    [SerializeField] Transform destinationPoint;
    [SerializeField] private float timeToTeleport = 1f;

    bool goToPos;

    private void Awake()
    {
        if (playerObject == null)
        {
            playerObject = GameObject.Find("Player");
        }
        if (currentObject == null)
        {
            currentObject = gameObject.transform.parent.gameObject;
        }
        if (destinationPoint == null)
        {
            destinationPoint = currentObject.transform.GetComponentInChildren<Teleporter>().gameObject.transform.GetChild(0);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            TransitionFader.faderFx();

            //GoToPosition(playerObject, destinationPoint,true);
            StartCoroutine(WaitToTeleport(timeToTeleport));
        }
    }

    private void Update()
    {
        if (goToPos)
        {
            playerObject.transform.SetPositionAndRotation(destinationPoint.transform.position, Quaternion.Euler(destinationPoint.transform.eulerAngles));
        }
    }
    public void GoToPosition(GameObject objectToTeleport, Transform destinationPos, bool waitToTeleport = false)
    {
        playerObject = objectToTeleport;
        destinationPoint = destinationPos;

        if (waitToTeleport)
        {
            StartCoroutine(WaitToTeleport(0.5f));
        }
        else
        {
            StartCoroutine(WaitToTeleport(0f));

        }
    }

    IEnumerator WaitToTeleport(float timeToWait)
    {
        yield return new WaitForSeconds(timeToWait);
        destinationObject.SetActive(true);
        goToPos = true;
        yield return new WaitForSeconds(timeToWait);
        goToPos = false;
        if (!keepCurrentObject)
        {
            currentObject.SetActive(false);
        }
        Debug.Log("Finaliza");

    }
}
