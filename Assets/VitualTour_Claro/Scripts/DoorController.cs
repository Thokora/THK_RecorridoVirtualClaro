﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : MonoBehaviour
{
    [SerializeField] private bool hasAnim;
    [SerializeField] private GameObject DoorWithAnim;
    private Animator doorAnim;

    private void Awake()
    {
        if (DoorWithAnim == null)
        {
            DoorWithAnim = gameObject;
        }

        if (hasAnim)
        {
            doorAnim = DoorWithAnim.GetComponent<Animator>();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (hasAnim)
            {
                doorAnim.Play("Door_open");
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (hasAnim)
            {
                doorAnim.Play("Door_close");
            }
        }
    }
}
