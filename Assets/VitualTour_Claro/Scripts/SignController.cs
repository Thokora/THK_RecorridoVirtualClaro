﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SignController : MonoBehaviour
{
    [Header("Choose your sign")]
    [SerializeField] private Signs signs;

    [Header("No Touch")]
    [SerializeField] private GameObject signImage;
    [SerializeField] private Sprite[] sprtSign;
    private Animator signAnim;
    private Image signImg;

    private void Start()
    {
        signAnim = signImage.GetComponent<Animator>();
        signImg = signImage.GetComponent<Image>();

        Debug.Log((int)signs);

        signImg.sprite = sprtSign[(int)signs];
    }

        private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            signAnim.SetBool("Open", true);
        }
        
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            signAnim.SetBool("Open", false);
        }
    }
}